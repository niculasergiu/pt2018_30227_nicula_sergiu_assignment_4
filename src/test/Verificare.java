package test;
import static org.junit.Assert.*;

import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import org.junit.Test;

public class Verificare {

    @Test
    public void Verificcare() throws Exception {

        Testare testare = new Testare();

        Person p1=new Person(1,"bula","312321312","alba");
        Person p2=new Person(2,"bula","312321312","alba");
        SavingAccount a1 = new SavingAccount(1,p1.getId(),600,0.05);
        SpendingAccount a2 = new SpendingAccount(2,p2.getId(),600);

        assertTrue(testare.validareCreateA(p1));
        assertTrue(testare.validareCreateA(p2));

        assertTrue(testare.validareSavingAccount(p1,a1));
        assertTrue(testare.validareSpendingAccount(p1,a2));

        assertTrue(testare.validareUpdate(p1,a2.getId(),1,200,800));
        assertTrue(testare.validareUpdate(p1,a2.getId(),2,200,600));


        assertTrue(testare.validareDeleteA(p1,a1,1));
        assertTrue(testare.validareDeleteP(p2,2));

    }

}

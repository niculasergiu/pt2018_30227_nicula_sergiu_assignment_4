package test;
import java.util.*;
import model.*;

public class Testare {
    Bank bank = new Bank("BT");

    public boolean validareCreateA(Person person) throws Exception {
        bank.adaugaClient(person);
        Person person1 = bank.afisareClient(person.getId());
        if (person.getId() == person1.getId()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validareSavingAccount(Person person, Account account) throws Exception {
        bank.adaugaCont(person, account);
        Account account1 = bank.afisareCont(person, account.getId());
        if (account1.getTip() == 1 && account.getId() == account1.getId() && account1.getDobanda() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validareSpendingAccount(Person person, Account account) throws Exception {
        bank.adaugaCont(person, account);
        Account account1 = bank.afisareCont(person, account.getId());
        if (account1.getTip() == 2 && account.getId() == account1.getId() && account1.getDobanda() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validareUpdate(Person person, int id,int op,int sumaOp,int rezultatP) throws Exception {
        double s=bank.upCont(person,id,op,sumaOp);
        if(s==rezultatP){
            return true;
        }
        else
            return false;
    }
    public boolean validareDeleteA(Person person, Account account,int idV) throws Exception {
        int id = bank.stergeCont(person, account.getId());
        if(idV==id){
            return true;
        }
        else
            return false;
    }

    public boolean validareDeleteP(Person person,int idV) throws Exception {
        int id = bank.stergeClient(person.getId());
        if(id==idV){
            return true;
        }
        else{
            return false;
        }
    }

}

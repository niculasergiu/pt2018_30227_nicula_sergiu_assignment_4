package interfata;


import model.*;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        Bank bank = new Bank("BT");
        Person person = new Person(1,"bula","312321312","alba");

        SpendingAccount account4 = new SpendingAccount(1,person.getId(),600);
        bank.adaugaClient(person);
        bank.adaugaCont(bank.afisareClient(person.getId()),account4);

        bank.serializare();
    }
}

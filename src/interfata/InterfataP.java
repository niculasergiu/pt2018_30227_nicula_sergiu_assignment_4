package interfata;
import javafx.stage.Window;
import model.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class InterfataP extends JFrame {

    private Bank bank = new Bank("BT");


    private JLabel scris1 = new JLabel("PERSON");
    private JLabel scris = new JLabel("ACCOUNT");
    private JTable tableA;
    private JTable tableP;

    private JTextField t1= new JTextField("id");
    private JTextField t2= new JTextField("suma initiala");
    private JTextField t3= new JTextField("dobanda");
    private JTextField t4= new JTextField("Operatie");
    private JTextField t5= new JTextField("Suma");
    private JTextField t6= new JTextField("idC");

    private JButton b1= new JButton("CREATE SAVING");
    private JButton b2= new JButton("CREATE SPEDING");
    private JButton b3= new JButton("DELETE");
    private JButton b4= new JButton("UPDATE");
    private JButton b5= new JButton("VIEW");


    private JTextField t11= new JTextField("id");
    private JTextField t21= new JTextField("nume");
    private JTextField t31= new JTextField("CNP");
    private JTextField t41= new JTextField("adresa");

    private JButton b11= new JButton("CREATE");
    private JButton b21= new JButton("DELETE");
    private JButton b31= new JButton("UPDATE");
    private JButton b41= new JButton("VIEW");
    private JButton b51= new JButton("SELECT");

    private int sP;

    public InterfataP(String title){
        setTitle(title);
        setSize(2000,800);//dimensiune interfata
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);
        setResizable(false);
        panel.setEnabled(true); ///sa il putem folosii si vedea
        setContentPane(panel);
        setVisible(true);

        setVisible(false);
        Vector<Object> ob = new Vector<Object>();
        List<Account> p = new ArrayList<Account>();
        try {
            p=bank.afisareConturi();

            for(Object a : p ){
                ob.add(a);
            }
            tableA = ReflectionT.createTable(ob,1);
            JScrollPane jsA = new JScrollPane(tableA);
            jsA.setBounds(1400, 50, 500, 300);
            panel.add(jsA);
            setVisible(true);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        setVisible(false);
        Vector<Object> ob1 = new Vector<Object>();
        List<Person> p1 = new ArrayList<Person>();
        try {
            p1=bank.afisareClienti();

            for(Object a : p1 ){
                ob1.add(a);
            }
            tableP = ReflectionT.createTable(ob1,2);
            JScrollPane jsP = new JScrollPane(tableP);
            jsP.setBounds(1400, 400, 500, 300);
            panel.add(jsP);
            setVisible(true);
        } catch (Exception e1) {
            e1.printStackTrace();
        }



        Font buton = new Font("", Font.BOLD, 18);
        Font mesaj = new Font("", Font.BOLD, 25);
        Font txt = new Font("", Font.BOLD, 12);

        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        scris1.setHorizontalAlignment(JLabel.CENTER);
        scris1.setFont(mesaj);

        t1.setHorizontalAlignment(JLabel.CENTER);
        t1.setFont(txt);
        t2.setHorizontalAlignment(JLabel.CENTER);
        t2.setFont(txt);
        t3.setHorizontalAlignment(JLabel.CENTER);
        t3.setFont(txt);
        t4.setHorizontalAlignment(JLabel.CENTER);
        t4.setFont(txt);
        t5.setHorizontalAlignment(JLabel.CENTER);
        t5.setFont(txt);
        t6.setHorizontalAlignment(JLabel.CENTER);
        t6.setFont(txt);


        b1.setHorizontalAlignment(JLabel.CENTER);
        b1.setFont(buton);
        b2.setHorizontalAlignment(JLabel.CENTER);
        b2.setFont(buton);
        b3.setHorizontalAlignment(JLabel.CENTER);
        b3.setFont(buton);
        b4.setHorizontalAlignment(JLabel.CENTER);
        b4.setFont(buton);
        b5.setHorizontalAlignment(JLabel.CENTER);
        b5.setFont(buton);


        t11.setHorizontalAlignment(JLabel.CENTER);
        t11.setFont(txt);
        t21.setHorizontalAlignment(JLabel.CENTER);
        t21.setFont(txt);
        t31.setHorizontalAlignment(JLabel.CENTER);
        t31.setFont(txt);
        t41.setHorizontalAlignment(JLabel.CENTER);
        t41.setFont(txt);

        b11.setHorizontalAlignment(JLabel.CENTER);
        b11.setFont(buton);
        b21.setHorizontalAlignment(JLabel.CENTER);
        b21.setFont(buton);
        b31.setHorizontalAlignment(JLabel.CENTER);
        b31.setFont(buton);
        b41.setHorizontalAlignment(JLabel.CENTER);
        b41.setFont(buton);
        b51.setHorizontalAlignment(JLabel.CENTER);
        b51.setFont(buton);


        scris.setBounds(0,50,600,60);
        scris1.setBounds(700,50,600,60);


        t1.setBounds(100,220,100,40);
        t2.setBounds(100,290,100,40);
        t3.setBounds(100,360,100,40);
        t4.setBounds(100,430,100,40);
        t5.setBounds(100,500,100,40);
        t6.setBounds(100,570,100,40);

        b1.setBounds(350,200,200,60);
        b2.setBounds(350,275,200,60);
        b3.setBounds(350,350,200,60);
        b4.setBounds(350,425,200,60);
        b5.setBounds(350,500,200,60);



        t11.setBounds(800,220,100,40);
        t21.setBounds(800,290,100,40);
        t31.setBounds(800,360,100,40);
        t41.setBounds(800,430,100,40);

        b11.setBounds(1050,200,200,60);
        b21.setBounds(1050,275,200,60);
        b31.setBounds(1050,350,200,60);
        b41.setBounds(1050,500,200,60);
        b51.setBounds(1050,425,200,60);

        panel.add(scris);


        panel.add(t1);
        panel.add(t2);
        panel.add(t3);
        panel.add(t4);
        panel.add(t5);
        panel.add(t6);

        panel.add(b1);
        panel.add(b2);
        panel.add(b3);
        panel.add(b4);
       // panel.add(b5);


        panel.add(scris1);


        panel.add(t11);
        panel.add(t21);
        panel.add(t31);
        panel.add(t41);

        panel.add(b11);
        panel.add(b21);
        panel.add(b31);
        //panel.add(b41);
        panel.add(b51);



        b1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                SavingAccount account = new SavingAccount(Integer.parseInt(t1.getText()),Integer.parseInt(t6.getText()),Double.parseDouble(t2.getText()),Double.parseDouble(t3.getText()));
                try {
                    bank.adaugaCont(bank.afisareClient(Integer.parseInt(t6.getText())),account);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                SpendingAccount account = new SpendingAccount(Integer.parseInt(t1.getText()),Integer.parseInt(t6.getText()),Double.parseDouble(t2.getText()));
                try {
                    bank.adaugaCont(bank.afisareClient(Integer.parseInt(t6.getText())),account);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    bank.stergeCont(bank.afisareClient(Integer.parseInt(t6.getText())),Integer.parseInt(t1.getText()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    bank.upCont(bank.afisareClient(Integer.parseInt(t6.getText())),Integer.parseInt(t1.getText()),Integer.parseInt(t4.getText()),Integer.parseInt(t5.getText()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                Vector<Object> ob = new Vector<Object>();
                List<Account> p = new ArrayList<Account>();
                try {
                    p=bank.afisareConturi();

                    for(Object a : p ){
                        ob.add(a);
                    }
                    tableA = ReflectionT.createTable(ob,1);
                    JScrollPane jsA = new JScrollPane(tableA);
                    jsA.setBounds(1400, 50, 500, 300);
                    panel.add(jsA);
                    setVisible(true);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });



        b11.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                Person person = new Person(Integer.parseInt(t11.getText()),t21.getText(),t31.getText(),t41.getText());
                try {
                    bank.adaugaClient(person);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b21.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    bank.stergeClient(Integer.parseInt(t11.getText()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b31.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                Person person = new Person(Integer.parseInt(t11.getText()),t21.getText(),t31.getText(),t41.getText());
                try {
                    bank.upPerson(person);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b41.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
                Vector<Object> ob = new Vector<Object>();
                List<Person> p = new ArrayList<Person>();
                try {
                    p=bank.afisareClienti();

                    for(Object a : p ){
                        ob.add(a);
                    }
                    tableP = ReflectionT.createTable(ob,2);
                    setVisible(true);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        b51.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                t6.setText(sP+"");
            }
        });

        tableP.setCellSelectionEnabled(true);
        ListSelectionModel cellSelectionModel = tableP.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {


                int[] selectedRow = tableP.getSelectedRows();
                int[] selectedColumns = tableP.getSelectedColumns();

                for (int i = 0; i < selectedRow.length; i++) {
                    for (int j = 0; j < selectedColumns.length; j++) {
                        sP = (Integer) tableP.getValueAt(selectedRow[i], selectedColumns[j]);
                    }
                }
            }

        });

        b51.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    bank.afisareClient(sP);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        });

        this.addWindowListener(new WindowListener() {
                                   @Override
                                   public void windowOpened(WindowEvent e) {

                                   }

                                   @Override
                                   public void windowClosing(WindowEvent e) {
                                       bank.serializare();
                                   }

                                   @Override
                                   public void windowClosed(WindowEvent e) {
                                   }

                                   @Override
                                   public void windowIconified(WindowEvent e) {

                                   }

                                   @Override
                                   public void windowDeiconified(WindowEvent e) {

                                   }

                                   @Override
                                   public void windowActivated(WindowEvent e) {

                                   }

                                   @Override
                                   public void windowDeactivated(WindowEvent e) {

                                   }
                               });
    }


    public static void main(String[] args) {
        new InterfataP ("BANK");
    }
}



package model;

import model.Account;
import model.Person;

public class SpendingAccount extends Account {
    public SpendingAccount(int id, int idC, double suma) {
        super(id, idC, suma, 2,0);
    }
    public String toString() {
        return "SpendingAccount{" +
                "id=" + super.getId() +
                ", idC=" + super.getIdC() +
                ", suma=" + super.getSuma() +
                '}';
    }
}

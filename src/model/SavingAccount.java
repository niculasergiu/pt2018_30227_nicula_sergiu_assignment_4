package model;

import model.Account;
import model.Person;

public class SavingAccount extends Account {

    public SavingAccount(int id, int idC, double suma,double dobanda) {
        super(id, idC, suma, 1,dobanda);
    }
    public String toString() {
        return "SavingAccount{" +
                "id=" + super.getId() +
                ", idC=" + super.getIdC() +
                ", suma=" + super.getSuma() +
                '}';
    }
}


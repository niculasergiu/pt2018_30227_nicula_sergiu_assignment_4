package model;

import java.util.HashMap;
import java.util.List;

public interface BankProc {
/*
  Functia isWellFormed() verfica toate datele din hashMap la inceputul metodei(preconditie) si la sfarsitul metodei (postconditie)
  pentru a nu putea avea date invalide care sa le accesam in interiorul metodelor
 */
 public void adaugaClient(Person person) throws Exception;
 public int stergeClient(int id) throws Exception;
 public void adaugaCont(Person person,Account account) throws Exception;
 public int  stergeCont(Person person,int id) throws Exception;
 public Person afisareClient(int id) throws Exception;
 public List<Person> afisareClienti() throws Exception;
 public Account afisareCont(Person person,int id) throws Exception;
 public List<Account> afisareConturi() throws Exception;
 public double upCont(Person person, int id, int op, int suma) throws Exception;
 public void upPerson(Person person)throws Exception;
 public HashMap<Person, List<Account>> getHaspMap();
}

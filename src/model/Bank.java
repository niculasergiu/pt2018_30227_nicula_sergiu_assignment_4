package model;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.*;
import java.io.*;
public class Bank implements BankProc {
    private HashMap<Person, List<Account>> hashMap;
    private String nume;

    public Bank(String nume) {
        hashMap = new HashMap<Person,List<Account>>();
        this.nume = nume;
        //deserializare();
    }
    public HashMap<Person, List<Account>> getHaspMap(){
        assert isWellFormed();
        return this.hashMap;
    }

    public int getIdc(int a){
        int index=0;
            for(Person person : hashMap.keySet()){
                if(index==a){
                    return person.getId();
                }
                index++;
            }

         return -1;
    }
    public void adaugaClient(Person person) throws Exception {
        assert isWellFormed();
        int ok=0;
        for(Person personT : hashMap.keySet()) {
            if (person.getId() == personT.getId() && person.getId()>0) {
                ok = 1;
            }
        }
        if(ok==0) {
            hashMap.put(person, new ArrayList<>());
            System.out.println("Create "+person.toString());
        }
        else {
            throw new Exception("id existent");
        }
        assert isWellFormed();
    }

    public int stergeClient(int id) throws Exception {
        assert isWellFormed();
        int ok =0;
        int idV=0;
        for(Person person : hashMap.keySet()){
            if(person.getId()==id && hashMap.get(person).size()==0)
            {
                idV=person.getId();
                hashMap.remove(person);
                System.out.println("Delete "+person.toString());
                ok=1;
                break;
            }
        }
        if(ok==0){
            throw new Exception("nu se poate efectua stergerea");
        }
        assert isWellFormed();
        return idV;
    }

    public void adaugaCont(Person person,Account account) throws Exception {
        assert isWellFormed();
        int ok=0;
        for(Account account1: hashMap.get(person)){
            if(account1.getId()==account.getId()){
                ok=1;
            }
        }
        if(ok==1){
            throw new Exception("id existent");
        }
        if(account.getTip()!=1 && account.getTip()!=2){
            throw  new Exception("invalid tipul de cont");
        }
        if(account.getTip()==1){
            if(account.getSuma()<200){
                throw  new Exception("Suma minima pentru SavingAccount este de 200");
            }
            account.setSuma(account.getSuma()+account.getSuma()*account.getDobanda());
            hashMap.get(person).add(account);
            System.out.println("Create "+ account.toString());
        }
        else{
            if(account.getSuma()<0){
                throw  new Exception("Suma invalida");
            }
            hashMap.get(person).add(account);
            System.out.println("Create "+ account.toString());
        }
        assert isWellFormed();
    }
    public int stergeCont(Person person,int id) throws Exception {
        assert isWellFormed();
        int ok =0;
        int index=0;
        int idA=0;
        for(Account account : hashMap.get(person)){
            if(account.getId()==id)
            {
                System.out.println( "Delete " + hashMap.get(person).get(index).toString());
                idA=account.getId();
                hashMap.get(person).remove(index);
                ok=1;
                break;
            }
            if(ok==0) {
                index++;
            }
        }
        if(ok==0){
            throw new Exception("nu se poate efectua stergerea");
        }
        assert isWellFormed();
        return idA;
    }
    public Person afisareClient(int id) throws Exception {
        assert isWellFormed();
        int ok=0;
        for(Person personT : hashMap.keySet()) {
            if (id == personT.getId()) {
               assert isWellFormed();
               return  personT;
            }
        }
        if(ok==0) {
            throw new Exception("Nu exista clientul cu id "+id);
        }
        assert isWellFormed();
        return new Person();
    }
    public List<Person> afisareClienti() throws Exception {
        assert isWellFormed();
        if( hashMap.keySet().size()==0){
            throw new Exception("nu exista persoane");
        }
        List<Person> view = new ArrayList<Person>();
        for(Person person : hashMap.keySet()){
            view.add(person);
        }
        assert isWellFormed();
        return view;
    }

    public Account afisareCont(Person person,int id) throws Exception {
        assert isWellFormed();
        int ok=0;
            for(Account account : hashMap.get(person)){
                if(id==account.getId()) {
                    return account;
                }
        }
        if(ok==0){
            throw new Exception("Nu exista la persoana "+person.getNume()+" cont cu id "+id);
        }
        assert isWellFormed();
        return new Account();
    }
    public List<Account> afisareConturi() throws Exception {
        assert isWellFormed();
        if( hashMap.keySet().size()==0){
            throw new Exception("nu exista conturi");
        }
        List<Account> view = new ArrayList<Account>();
        for(Person person : hashMap.keySet()){
            for(Account account : hashMap.get(person)){
                view.add(account);
            }
        }
        assert isWellFormed();
        return view;
    }
    public void upPerson(Person person)throws Exception{
        assert isWellFormed();
        int ok=0;
        for(Person personT : hashMap.keySet()) {
            if (person.getId() == personT.getId()) {
                personT.setNume(person.getNume());
                personT.setCNP(person.getCNP());
                personT.setAdresa(person.getAdresa());
                System.out.println("Update "+person.toString());
                ok = 1;
            }

        }
        if (ok==0){
            throw new Exception("id negasit");
        }
        assert isWellFormed();
    }
    public double upCont(Person person, int id, int op, int suma) throws Exception {
        int ok=0;
        int index=-1;
        for(Person person1 : hashMap.keySet()){
            if(person.getId()==person1.getId()){
                for(Account account : hashMap.get(person)){
                    if(ok==0) {
                        index++;
                    }
                    if(account.getId()==id){
                        ok=1;
                    }
                }
            }
        }
        if(ok==0){
            throw new Exception("nu exista cont");
        }
        if(hashMap.get(person).get(index).getTip()==1){
            throw new Exception("nu putem face update pe Saving Account");
        }
        if(op!=1 && op!=2) {
            throw new Exception("nu exista operatie");
        }
        if(op==1){
            hashMap.get(person).get(index).setSuma(hashMap.get(person).get(index).getSuma()+suma);
            System.out.println("Update "+ hashMap.get(person).get(index).toString()+" (depunere)");
        }
        if(op==2){
            if(suma>hashMap.get(person).get(index).getSuma()){
                throw new Exception("Fondurii insuficiente!!!");
            }
            else{
                hashMap.get(person).get(index).setSuma(hashMap.get(person).get(index).getSuma()-suma);
                System.out.println("Update "+ hashMap.get(person).get(index).toString()+" (extragere)");
            }
        }
        assert isWellFormed();
        return hashMap.get(person).get(index).getSuma();
    }
    public void serializare(){
        try
        {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Bank.bin"));

            out.writeObject(hashMap);
            out.close();
        }
        catch (IOException i)
        {
            i.printStackTrace();
        }

    }

    public void deserializare() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("Bank.bin"));
            hashMap = (HashMap<Person, List<Account>>) in.readObject();
            in.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Bank not found");
            c.printStackTrace();
            return;

        }
    }
    public boolean isWellFormed()
    {
        for(Person person : hashMap.keySet())
        {
            for(Field field : person.getClass().getDeclaredFields()){
                if(field==null){
                    return false;
                }
                for(Account account: hashMap.get(person)){
                    for(Field field1 : account.getClass().getDeclaredFields()){
                        if(field1==null)
                        {
                            return false;
                        }
                    }
                }
            }

        }
        return true;
    }
}

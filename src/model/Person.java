package model;

import java.io.Serializable;

public class Person implements Serializable {
    private int id;
    private String nume;
    private String CNP;
    private String adresa;

    public Person(int id, String nume, String CNP, String adresa) {
        this.id = id;
        this.nume = nume;
        this.CNP = CNP;
        this.adresa = adresa;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nume=" + nume +
                ", CNP='" + CNP + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
}

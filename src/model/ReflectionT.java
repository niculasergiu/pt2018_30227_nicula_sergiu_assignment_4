package model;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.util.*;

public class ReflectionT {

    public static JTable createTable(Vector<Object> object,int j) {
        Vector<String> capTabel = new Vector<String>();
        Vector<Vector> el = new Vector<Vector>();
        if(j==1) {
            for (Field field : object.get(0).getClass().getSuperclass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    capTabel.add(field.getName());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
            int size = object.size();
            Object val;
            for (int i = 0; i < size; i++) {
                Vector<Object> obj = new Vector<Object>();
                for (Field field : object.get(i).getClass().getSuperclass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        val = field.get(object.get(i));
                        obj.add(val);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                el.add(obj);
            }
            DefaultTableModel table = new DefaultTableModel(capTabel, 0);
            for (int i = 0; i < el.size(); i++) {
                table.addRow(el.get(i));
            }
            return new JTable(table);
        }
        else{
            for (Field field : object.get(0).getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    capTabel.add(field.getName());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
            int size = object.size();
            Object val;
            for (int i = 0; i < size; i++) {
                Vector<Object> obj = new Vector<Object>();
                for (Field field : object.get(i).getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    try {
                        val = field.get(object.get(i));
                        obj.add(val);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                el.add(obj);
            }
            DefaultTableModel table = new DefaultTableModel(capTabel, 0);
            for (int i = 0; i < el.size(); i++) {
                table.addRow(el.get(i));
            }
            return new JTable(table);

        }
    }
}
package model;

import java.io.Serializable;

public class Account implements Serializable{
    private int id;
    private int idC;
    private double suma;
    private int tip;
    private double dobanda;

    public Account(int id, int idC, double suma, int tip,double dobanda) {
        this.id = id;
        this.idC = idC;
        this.suma = suma;
        this.tip = tip;
        this.dobanda = dobanda;
    }

    public Account(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public double getSuma() {
        return suma;
    }

    public void setSuma(double suma) {
        this.suma = suma;
    }

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public double getDobanda() {
        return dobanda;
    }

    public void setDobanda(double dobanda) {
        this.dobanda = dobanda;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }
    
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", idC=" + idC +
                ", suma=" + suma +
                ", tip=" + tip +
                ", dobanda=" + dobanda +
                '}';
    }
}
